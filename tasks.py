from invoke import task
from rich import print

from ipfabric_snow.utils.servicenow_client import Snow
from ipfabric_snow.apps.env_setup import ensure_environment_is_setup
from ipfabric_snow.apps.env_setup import get_auth


@task
def clear_netgear_table(c):
    env_vars = ensure_environment_is_setup()
    auth = get_auth(env_vars)
    snow = Snow(auth=auth, url=env_vars["SNOW_URL"])
    snow_netgear_list = snow.cmdb.net_gear.get_all()["result"]
    for netgear_ci in snow_netgear_list:
        ci_sys_id = netgear_ci["sys_id"]
        snow.cmdb.net_gear.delete_ci(ci_sys_id=ci_sys_id)
        print(f"Deleted CI: {netgear_ci['name']}")
