---
description: This section is about how to use and operate the ServiceNow and IP Fabric integration.
---

# Using the `ipfabric-snow` CLI

## Main Commands

### Syncing

```shell
❯ ipfabric-snow sync --help
                                                                                                                                                                                                                                                             
 Usage: ipfabric-snow sync [OPTIONS] COMMAND [ARGS]...                                                                                                                                                                                                       
                                                                                                                                                                                                                                                             
 Sync Inventory data with ServiceNow                                                                                                                                                                                                                        
                                                                                                                                                                                                                                                             
╭─ Options ─────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╮
│ --help          Show this message and exit.                                                                                                                                                                                                               │
╰───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╯
╭─ Commands ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╮
│ devices                                  Sync devices from IP Fabric to ServiceNow                                                                                                                                                                        │
╰───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╯
```

#### Example of a Complex `sync devices` Command

```shell
ipfabric-snow --log-level DEBUG sync devices --show-diff --diff-source SNOW  --ipf-snapshot "12dd8c61-129c-431a-b98b-4c9211571f89" --output-verbose --timeout 30
```

### `ipfabric-snow env setup` Usage

```shell
❯ ipfabric-snow env setup
2023-11-30 20:33:10.463 | INFO     | ipfabric_snow.apps.env_setup:setup_environment:75 - Setting up the environment
2023-11-30 20:33:10.464 | INFO     | ipfabric_snow.utils.env_manager:read_env_file:17 - env file path: ipfabric-snow/.env
Username and password or token for SNOW?
Please enter 'user' or 'token'. Default is token.
 [user/token] (token): user
Please enter the SNOW_USER value: admin
Please enter the SNOW_PASS value:
Please enter the IPF_URL value: https://sa-eu-demo02a.hel1-cloud.ipf.cx
Please enter the SNOW_URL value: https://dev88344.service-now.com
Do you want to store sensitive data (passwords, tokens) in the .env file? [y/N]: y
2023-11-30 20:34:25.443 | INFO     | ipfabric_snow.utils.env_manager:write_env_file:33 - env file path: ipfabric-snow/.env
2023-11-30 20:34:25.450 | INFO     | ipfabric_snow.apps.env_setup:setup_environment:99 - Environment setup complete
```

## `ipfabric-snow syncing devices` Options

To view all syncing devices options, run `ipfabric-snow sync devices --help`:

```shell
❯ ipfabric-snow sync devices --help
                                                                                                                                                                                                                                                             
 Usage: ipfabric-snow sync devices [OPTIONS] [STAGING_TABLE_NAME]                                                                                                                                                                                            
                                                                                                                                                                                                                                                             
 Sync devices from IP Fabric to ServiceNow                                                                                                                                                                                                                  
                                                                                                                                                                                                                                                             
╭─ Arguments ───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╮
│   staging_table_name      [STAGING_TABLE_NAME]  The name of the ServiceNow staging table to use. [env var: SNOW_STAGING_TABLE_NAME] [default: x_1249630_ipf_devices]                                                                                      │
╰───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╯
╭─ Options ─────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╮
│ --show-diff         --no-show-diff                  Display the data difference [default: no-show-diff]                                                                                                                                                   │
│ --diff-source                              TEXT     Specify the main source for diff, either IPF or SNOW [default: IPF]                                                                                                                                   │
│ --write-diff        --no-write-diff                 Enable or disable writing the diff to a file [default: no-write-diff]                                                                                                                                  │
│ --diff-file                                TEXT     Path to save the diff file, if desired [default: data/{date_time}_diff_{diff_source}.json]                                                                                                              │
│ --dry-run           --no-dry-run                    Perform a dry run without making any changes [default: no-dry-run]                                                                                                                                    │
│ --ipf-snapshot                             TEXT     IP Fabric snapshot ID to use for the sync [default: $last]                                                                                                                                            │
│ --timeout                                  INTEGER  timeout for httpx requests [default: 10]                                                                                                                                                              │
│ --output-verbose    --no-output-verbose             adds more detail to the output. Identifies which keys changed per device [default: no-output-verbose]                                                                                                  │
│ --help                                              Show this message and exit.                                                                                                                                                                           │
╰───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╯
```

### `ipfabric-snow sync devices` Options Examples

If you wanted to sync devices from IP Fabric to ServiceNow:

```shell
❯ ipfabric-snow sync devices
```

Sync devices from IP Fabric to ServiceNow and show the diff:

```shell
❯ ipfabric-snow sync devices --show-diff
```

Only show the difference of devices in IP Fabric compared to ServiceNow (ensuring devices are not pushed into ServiceNow):

```shell
❯ ipfabric-snow sync devices --show-diff --dry-run
```

If you wanted to only show the difference of devices in ServiceNow compared to IP Fabric (ensuring devices are not pushed into ServiceNow):

```shell
❯ ipfabric-snow sync devices --show-diff --dry-run --diff-source SNOW
```

Write the diff to a file:

```shell
❯ ipfabric-snow sync devices --show-diff --write-diff
```

Want to write the diff to a file with a custom name?

```shell
❯ ipfabric-snow sync devices --show-diff --write-diff --diff-file custom_name.json
```

## Logging Options

To view all logging options, run `ipfabric-snow --help`:

```shell
❯ ipfabric-snow --help

 Usage: ipfabric-snow [OPTIONS] COMMAND [ARGS]...

╭─ Options ─────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╮
│ --log-level                                 TEXT  Log level [default: INFO]                                                                                   │
│ --log-to-file           --no-log-to-file          Log to file [default: log-to-file]                                                                          │
│ --log-file-name                             TEXT  Log file name [default: ipf_serviceNow.log]                                                                 │
│ --log-json              --no-log-json             Log in JSON format [default: no-log-json]                                                                   │
│ --install-completion                              Install completion for the current shell.                                                                   │
│ --show-completion                                 Show completion for the current shell, to copy it or customize the installation.                            │
│ --help                                            Show this message and exit.                                                                                 │
╰───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╯
```

### Logging Options Examples

If you wanted to modify the log level to `DEBUG`, you would run the following command:

```shell
❯ ipfabric-snow --log-level DEBUG sync devices
```

If you wanted to log to a file, you would run the following command:

```shell
❯ ipfabric-snow --log-to-file sync devices
```

If you wanted to log to a file with a custom name, you would run the following command:

```shell
❯ ipfabric-snow --log-to-file --log-file-name custom_name.log sync devices
```
