---
description: This section is about how to configure the ServiceNow and IP Fabric integration.
---

# Installation & Overview

```shell
❯ ipfabric-snow --help
                                                                                                                                                                                                                                                             
 Usage: ipfabric-snow [OPTIONS] COMMAND [ARGS]...                                                                                                                                                                                                            
                                                                                                                                                                                                                                                             
╭─ Options ─────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╮
│ --log-level                                 TEXT  Log level [default: INFO]                                                                                                                                                                               │
│ --log-to-file           --no-log-to-file          Log to file [default: log-to-file]                                                                                                                                                                      │
│ --log-file-name                             TEXT  Log file name [default: ipf_serviceNow.log]                                                                                                                                                             │
│ --log-json              --no-log-json             Log in JSON format [default: no-log-json]                                                                                                                                                               │
│ --install-completion                              Install completion for the current shell.                                                                                                                                                               │
│ --show-completion                                 Show completion for the current shell, to copy it or customize the installation.                                                                                                                        │
│ --help                                            Show this message and exit.                                                                                                                                                                             │
╰───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╯
╭─ Commands ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╮
│ env                            Setup environment variables                                                                                                                                                                                                │
│ sync                           Sync Inventory data with ServiceNow                                                                                                                                                                                        │
╰───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╯
```

## Installation

Requirements:

- Python 3.10+
- Access to IP Fabric and ServiceNow

### PyPI

```shell
pip install ipfabric-snow
```

### Poetry

Clone [the repository](https://gitlab.com/ip-fabric/integrations/ipfabric-snow) and run:

```shell
poetry install
```

## Overview

The `ipfabric-snow` integration is a powerful command-line tool designed to facilitate the synchronization of network inventory data between IP Fabric and ServiceNow. This Python application is built with minimal dependencies, ensuring ease of setup and maintenance. Key features and requirements of this tool include:

- **Staging Table Requirement:** `ipfabric-snow` requires a staging table in ServiceNow. This table acts as an intermediate location where data from IP Fabric is temporarily stored before being processed or transferred to the final destination table. In ServiceNow, this is referred to as a import set. Reference ServiceNow's documentation on [import sets](https://docs.servicenow.com/bundle/vancouver-integrate-applications/page/administer/import-sets/reference/import-sets-landing-page.html).

- **Transformation Maps:** The integration utilizes transformation maps, a component in ServiceNow, to map and transform data fields from IP Fabric to corresponding fields in ServiceNow. See ServiceNow's documentation on [transformation maps](https://docs.servicenow.com/csh?topicname=c_CreatingNewTransformMaps.html&version=latest).

- **Diffing with CMDB Network Gear Table:** The main functionality of `ipfabric-snow` is its ability to perform differential analysis between the data in IP Fabric and the existing data in ServiceNow's CMDB (Configuration Management Database) network gear table. This feature allows users to identify and synchronize any discrepancies between the two data sources.

- **Command-Line Interface:** `ipfabric-snow` is a command-line utility, offering a user-friendly interface with various commands and options. This approach makes it versatile for different environments and use cases, allowing automation and scripting.

- **Dry Run Feature:** Supports a "dry run" mode, which allows users to simulate the synchronization process without making actual changes.
