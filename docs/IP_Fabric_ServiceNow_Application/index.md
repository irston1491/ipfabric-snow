---
description: This section is about how to configure the ServiceNow application to work with IP Fabric.
---

# IP Fabric ServiceNow Application Installation and Management

## Installing From a Remote Repository

!!! warning

    This section requires access to the System Application Studio in ServiceNow. Please reference ServiceNow's documentation on [Importing an Application from Source Control](https://developer.servicenow.com/dev.do#!/learn/learning-plans/vancouver/new_to_servicenow/app_store_learnv2_devenvironment_vancouver_importing_an_application_from_source_control) for required roles and more information.

1. Navigate to the **System Application Studio**:

   ![System Application Studio](snow_studio.png)

2. Select **Import From Source Control** from the **File** menu:

   ![Import From Source Control](snow_import.png)

   !!! note

       When importing from source control, you need to a set of credentials to access the repository.
       Find ServiceNow's documentation on [setting credentials](https://docs.servicenow.com/csh?topicname=create-https-connection.html&version=latest).

3. Set the correct **URL** and **Branch**:

   ![Import Application](snow_url_and_branch.png)

   !!! note
   
       The **URL** and **Branch** should be set to the following:

       **URL:** `https://gitlab.com/ip-fabric/integrations/ipfabric-snow.git`

       **Branch:** `main_snow_app`

## Installed Tables

The application will install a import set table called `Devices`, which is viewable under the **IP Fabric** application menu.
Devices can be `Inserted`, `Ignored`, or `Skipped` based on the actions defined in the Transform Map.

![Devices table](snow_installed_table.png)

The name of the installed table is `x_1249630_ipf_devices`.

## Roles Installed by the Application

| Role Name                           |  
| ----------------------------------- |
| `x_1249630_ipf.ipf_cmdb_role_read`  |
| `x_1249630_ipf.ipf_cmdb_role_write` |
| `x 1249630_ipf.ipf_incident role`   |


## Granting Users Access to the IP Fabric Application in ServiceNow

1. Navigate to **User Administration --> Users**:

   ![User Administration - Users](snow_users_menu.png)

2. Select the user you wish to grant access to the IP Fabric application, and edit the user's roles:

   ![Edit user's roles](snow_user_roles.png)

3. Add the role `x_1249630_ipf.ipf_cmdb_role_read` or `x_1249630_ipf.ipf_cmdb_role_write` to the user:

   ![Add role to user](snow_edit_users_role.png)

## Updating the ServiceNow Application

1. Navigate to the **System Application Studio**

   ![System Application Studio](snow_studio.png)

2. Select the **IP Fabric** application:

   ![Select Application - IP Fabric](snow_select_app.png)

3. Select **Apply Remote Changes** from the **Source Control** menu:

   ![Apply Remote Changes menu option](snow_apply_remote_changes.png)

4. Confirm the changes with **Apply Remote Changes**:

   ![Apply Remote Changes confirmation](snow_confirm_changes.png)
